FROM php:7.4-fpm AS php-fpm

LABEL maintainer="adam_kirk@live.co.uk"

RUN apt-get update
RUN apt-get install -y apt-utils
RUN apt-get install -y zlib1g-dev
RUN apt-get install -y libzip-dev

RUN docker-php-ext-install zip

RUN apt-get install -y libpq-dev
RUN docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql
RUN docker-php-ext-install pdo
RUN docker-php-ext-install pdo_pgsql
RUN docker-php-ext-install pgsql
RUN docker-php-ext-install bcmath
RUN docker-php-ext-install sockets

RUN apt-get install -y libcurl3-dev
RUN apt-get install -y curl
RUN docker-php-ext-install curl

RUN apt-get install -y libonig-dev
RUN docker-php-ext-install mbstring

RUN apt-get install -y libpng-dev
RUN docker-php-ext-install gd

RUN apt-get install -y libxml2-dev
RUN docker-php-ext-install xml

COPY --from=composer:1.10 /usr/bin/composer /usr/bin/composer

RUN mkdir -p /var/www/.composer/cache

COPY ./entrypoint.sh /entrypoint.sh

RUN apt-get install -y git

RUN apt-get install -y libfcgi0ldbl

# Cleanup some files...probably needs to be more thorough
RUN apt-get clean all -y

COPY php-fpm.conf /usr/local/etc/
COPY php.ini /usr/local/etc/php
COPY php-fpm.d/* /usr/local/etc/php-fpm.d/
COPY entrypoint.sh /entrypoint.sh

RUN mkdir /opt/app-root

WORKDIR /opt/app-root

ENTRYPOINT ["/entrypoint.sh"]

FROM php-fpm AS dev

COPY dev-entrypoint.sh /dev-entrypoint.sh
RUN chmod +x /dev-entrypoint.sh

ENTRYPOINT ["/dev-entrypoint.sh"]

FROM cron AS cron-dev

COPY dev-entrypoint.sh /dev-entrypoint.sh
RUN chmod +x /dev-entrypoint.sh

ENTRYPOINT ["/dev-entrypoint.sh"]