.PHONY: build
build:
	docker build --target php-fpm -t php-fpm:test .

.PHONY: build-dev
build-dev:
	docker build --target dev -t php-fpm-dev:test .
